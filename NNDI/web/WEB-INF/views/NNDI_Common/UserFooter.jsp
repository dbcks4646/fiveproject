<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/nndi-style.css">
    <link href="${ pageContext.servletContext.contextPath }/resources/css/sidebars.css" rel="stylesheet" >
    <link href="https://fonts.googleapis.com/css2?family=Single+Day&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>너나들이중랑</title>
</head>
<body>
  <body>
    <div class="nav">
      <a href="../NNDI_Introduce/directions.html" style="margin-left: 15%;">오시는 길</a>
      <a href="../NNDI_Introduce/privacyPolicy.html">개인 정보 처리 방침</a>
    </div>
    <footer>
      <img src="${ pageContext.servletContext.contextPath }/resources/image/main_footer.png" class="nndi-foot">
    </footer>
    </body>
    </html>
    