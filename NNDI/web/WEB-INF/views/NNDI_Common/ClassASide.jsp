<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/nndi-style.css">
    <link href="${ pageContext.servletContext.contextPath }/resources/css/sidebars.css" rel="stylesheet" >
    <link href="https://fonts.googleapis.com/css2?family=Single+Day&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
   	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <title>너나들이중랑</title>
</head>
<body>

<aside>
	<nav>
	
		<div style="width: auto;">
			<a href="#" class="d-flex align-items-center pb-3 mb-3 link-dark text-decoration-none border-bottom">
 			<svg class="bi pe-none me-2" width="30" height="24"></svg>
  			<span class="fs-5 fw-semibold" style="color:rgb(255, 255, 255)">강좌 안내</span></a>
		</div>

		<button class="btn btn-c btn-toggle d-inline-flex align-items-center rounded border-0 collapsed" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="false">문화</button>
		<div class="collapse" id="home-collapse">
			<ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
    			<li><a href="#" class="link-dark d-inline-flex text-decoration-none rounded fw-semibold">아동</a></li>
    			<li><a href="#" class="link-dark d-inline-flex text-decoration-none rounded fw-semibold">청소년</a></li>
    			<li><a href="#" class="link-dark d-inline-flex text-decoration-none rounded fw-semibold">성인</a></li>
    			<li><a href="#" class="link-dark d-inline-flex text-decoration-none rounded fw-semibold">시니어</a></li>
  			</ul>
		</div>

		<br>
		
		<button class="btn btn-c btn-toggle d-inline-flex align-items-center rounded border-0 collapsed" data-bs-toggle="collapse" data-bs-target="#orders1-collapse" aria-expanded="false">체육</button>
		<div class="collapse" id="orders1-collapse">
			<ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
    			<li><a href="#" class="link-dark d-inline-flex text-decoration-none rounded fw-semibold">수영</a></li>
    			<li><a href="#" class="link-dark d-inline-flex text-decoration-none rounded fw-semibold">탁구</a></li>
    			<li><a href="#" class="link-dark d-inline-flex text-decoration-none rounded fw-semibold">농구</a></li>
    			<li><a href="#" class="link-dark d-inline-flex text-decoration-none rounded fw-semibold">배드민턴</a></li>
    			<li><a href="#" class="link-dark d-inline-flex text-decoration-none rounded fw-semibold">클라이밍</a></li>
  			</ul>
		</div>
		
	</nav>
</aside>

</body>
</html>