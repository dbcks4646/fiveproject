<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>너나들이중랑</title>
</head>
<body>

<aside>
    <nav>
        <div style="width: auto;">
            <a href="#" class="d-flex align-items-center pb-3 mb-3 link-dark text-decoration-none border-bottom">
                <svg class="bi pe-none me-2" width="30" height="24"></svg>
                <span class="fs-5 fw-semibold" style="color:rgb(255, 255, 255)">회원</span>
            </a>
        </div>

        <button class="btn btn-c btn-toggle d-inline-flex align-items-center rounded border-0 collapsed" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="false">회원</button>
        
        <div class="collapse show" id="home-collapse">
            <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                <li><a href="userIdFindMainPage.html" class="link-dark d-inline-flex text-decoration-none rounded fw-semibold">아이디 찾기</a></li>
                <li><a href="userPwdFindMainPage.html" class="link-dark d-inline-flex text-decoration-none rounded fw-semibold">비밀번호 찾기</a></li>
            </ul>
        </div>
    </nav>  
</side>

</body>
</html>